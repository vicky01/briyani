//import logo from "./logo.svg";
import "./App.css";
import  AppBar  from "./component/AppBar";
import  Banner from "./component/Banner";
import Comments from "./component/Comments";
import  Menu from "./component/Menu";
import PickoftheWeek from "./component/PickoftheWeek";
import  PizzaCard from "./component/PizzaCard";

function App() {
  return (
    <>
    <AppBar />
    <Banner />
    <Menu />
    <PickoftheWeek />
    <Comments />
    <PizzaCard />
    <div className="text-center my-3 text-primary small">Copyrights 2023-2024</div>
    </>
  );
}

export default App;
